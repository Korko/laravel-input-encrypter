<?php

namespace Korko\InputEncrypter;

use Collective\Html\FormBuilder;
use Korko\Encrypter\SymmetricalEncrypter as Encrypter;

class CryptedFormBuilder extends FormBuilder
{
    protected $key;

    public function __construct(HtmlBuilder $html, UrlGenerator $url, Factory $view, $csrfToken, $encryptionKey, Request $request = null)
    {
        parent::__construct($html, $url, $view, $csrfToken, $request);
        $this->key = $key;
    }

    /**
     * Create a form input field.
     *
     * @param  string $type
     * @param  string $name
     * @param  string $value
     * @param  array  $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function input($type, $name, $value = null, $options = [])
    {
        $name = (new Encrypter($this->key))->encrypt($name);

        return parent::input($type, $name, $value, $options);
    }
}
