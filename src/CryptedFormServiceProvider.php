<?php

namespace Korko\InputEncrypter;

use Illuminate\Support\ServiceProvider;

class CryptedFormServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CryptedFormBuilder::class, function ($app) {
            // Need to redefine the original instance because we want to override the "input" method even from internal calls
            // So copy the original instanciation but change the class name
            $form = new CryptedFormBuilder(
                $app['html'],
                $app['url'],
                $app['view'],
                $app['session.store']->token(),
                md5(csrf_token().config('app.key')),
                $app['request']
            );

            return $form->setSessionStore($app['session.store']);
        });
    }
}
